import { Component } from '@angular/core';
import * as xls from 'xlsx';
import { ExcelService } from '../../share/services/excel.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent {
  data: any[] = [];
  keys: string[] = []; // contains header
  correctAnswers: { [key: number]: number[] } = {}; // contains each row of correct answer
  errors: { [row: number]: string[] } = {};

  constructor(private excelService: ExcelService) {}

  readExcelFile(e: any) {
    const file = e.target.files[0];

    if (file) {
      this.excelService
        .readExcel(file)
        .then((data) => {
          this.normalizeData(data);
          this.processData(data);
          // this.data = data;
        })
        .catch((error) => {
          console.error('Error reading excel file', error);
        });
    }
  }

  // Method 1:
  // normalizeData(data: any[]) {
  //   // Find the longest row in data
  //   const maxColumns = data.reduce((max, row) => Math.max(max, row.length), 0);

  //   // Loop each row of columns
  //   data.forEach((row) => {
  //     while (row.length < maxColumns) {
  //       row.push('');
  //     }
  //   });
  // }

  // Method 2:
  normalizeData(data: any[]): any[][] {
    const maxColumns = Math.max(...data.map((row) => row.length));

    const normalizedData = data.map((row) => {
      const newRow = [...row];
      while (newRow.length < maxColumns) {
        newRow.push('');
      }
      return newRow;
    });
    return normalizedData;
  }

  addError(rowIndex: number, message: string) {
    if (!this.errors[rowIndex]) {
      this.errors[rowIndex] = []; // Initialize an array if it doesn't exist
    }
    this.errors[rowIndex].push(message); // Now you can safely use push
  }

  processData(data: any[]) {
    this.errors = [];
    if (data.length > 0) {
      const validColumns = data[0].reduce(
        (acc: number[], key: string, colIndex: number) => {
          const hasContent = data.some(
            (row) => row[colIndex] && row[colIndex].trim() !== ''
          );
          if (key.trim() !== '' && hasContent) {
            acc.push(colIndex);
          }
          return acc;
        },
        []
      );

      this.keys = validColumns.map((colIndex: any) => data[0][colIndex]);
      this.data = data
        .slice(1)
        .map((row) => validColumns.map((colIndex: any) => row[colIndex]));

      // Xác định cột "Đáp án đúng"
      const correctAnsColumnIndex = this.keys.indexOf('Đáp án đúng');
      if (correctAnsColumnIndex !== -1) {
        this.data?.forEach((row, rowIndex) => {
          const correctAns = row[correctAnsColumnIndex];
          if (correctAns) {
            const answerIndexes = correctAns
              .toString()
              .split(',')
              .map((ans: string) => parseInt(ans.trim(), 10));

            this.correctAnswers[rowIndex] = answerIndexes;

            answerIndexes.forEach((ansIndex: any) => {
              const answerColumnKey = `Câu trả lời ${ansIndex}`;
              const answerColumnIndex = this.keys.indexOf(answerColumnKey);

              if (answerColumnIndex === -1) {
                this.addError(
                  rowIndex,
                  `Hàng ${rowIndex + 1}: Không tồn tại cột "${answerColumnKey}"`
                );
              } else if (
                !row[answerColumnIndex] ||
                row[answerColumnIndex].trim() === ''
              ) {
                this.addError(
                  rowIndex,
                  `Hàng ${
                    rowIndex + 1
                  }: Ô nội dung của cột "${answerColumnKey}" rỗng`
                );
              }
            });
          }
          console.log('Processing data:', row);
        });
      }
    }
  }

  // isCorrectAnswer(rowIndex: number, columnIndex: number): boolean {
  //   const correctAns = this.correctAnswers[rowIndex];
  //   const columnKey = this.keys[columnIndex];
  //   const isAnswerColumn = columnKey && columnKey.startsWith('Câu trả lời');

  //   if (!isAnswerColumn) return false;

  //   const answerNumber = parseInt(columnKey.split(' ')[2]);
  //   if (isNaN(answerNumber)) return false;

  //   return correctAns ? correctAns.includes(answerNumber) : false;
  // }

  isCorrectAnswer(rowIndex: number, columnIndex: number): boolean {
    console.log(`Checking if correct: Row ${rowIndex}, Column ${columnIndex}`);

    // Check if there are errors for this specific row and column.
    if (this.errors[rowIndex] && this.errors[rowIndex].length > 0) {
      // Iterate over each error and check if it pertains to this column
      const errorForColumn = this.errors[rowIndex].some((error: any) => {
        return error.includes(`Câu trả lời ${columnIndex + 1}`); // Assuming error messages contain the column number as in your earlier setup
      });

      // If there's an error for this column, return false to avoid highlighting it
      if (errorForColumn) return false;
    }

    // Continue with existing checks
    const correctAns = this.correctAnswers[rowIndex];
    const columnKey = this.keys[columnIndex];
    if (!columnKey.startsWith('Câu trả lời')) return false;

    const answerNumber = parseInt(columnKey.split(' ')[2]);
    if (isNaN(answerNumber)) return false;

    console.log('Checking if correct:', answerNumber);

    return correctAns ? correctAns.includes(answerNumber) : false;
  }

  hasErrors(): boolean {
    return Object.keys(this.errors).length > 0;
  }

  getErrorMessages(): string[] {
    // Flatten the error messages into a single array
    return Object.values(this.errors).flat();
  }
}
