import { Component, ViewChild } from '@angular/core';
import { SharedModule } from '../../share/shared.module';
import { CommonModule } from '@angular/common';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
})
export class DashboardComponent {
  sidebarOpen = false;

  hanldeToggleSidenav(): void {
    this.sidebarOpen = !this.sidebarOpen;
  }
}
