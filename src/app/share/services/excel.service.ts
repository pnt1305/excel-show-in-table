import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root',
})
export class ExcelService {
  constructor() {}

  public readExcel(file: File): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        // translate in binary
        const bstr: string = e.target.result;
        // Xlxs library tranfer binary data to workbook object
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        // convert worksheet to json data array
        const data = XLSX.utils.sheet_to_json(ws, { header: 1 });
        resolve(data);
      };
      reader.onerror = (error) => {
        // call reject function if having error
        reject(error);
      };
      reader.readAsBinaryString(file);
    });
  }
}
